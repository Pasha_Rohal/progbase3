#ifndef RESURSAN_H
#define RESURSAN_H

#include <QDialog>

namespace Ui {
class resursan;
}

class resursan : public QDialog
{
    Q_OBJECT

public:
    explicit resursan(QWidget *parent = 0);
    ~resursan();

private slots:
    void on_information_clicked();

    void on_sickAn_clicked();

    void on_eatPb_clicked();

private:
    Ui::resursan *ui;
};

#endif // RESURSAN_H
