#include "safeanim.h"
#include "ui_safeanim.h"

#include <dialog.h>
#include <QMessageBox>
#include <safeanim.h>
#include <number_of_animals.h>
#include "bd.h"

safeAnim::safeAnim(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::safeAnim)
{
    ui->setupUi(this);
}

safeAnim::~safeAnim()
{
    delete ui;
}



void safeAnim::setD(int d)
{
    this->d=d;
}

void safeAnim::UpdateBase(int index)
{

    bd corn;
    number_of_Animals trk;
    int ID,weight,age;
    QString name,mf;

    corn.openDb();
    age =ui->ageLine->text().toInt();
    name = ui->nameLine->text();
    weight = ui->weightLine->text().toInt();
    mf = ui->maleLine->text();
    ID = ui->idLine->text().toInt();
    QSqlQuery qry;
       if(index == 1){
           qry = corn.bdAddSafeAnimals("insert into Lions (ID,name,age,weight,mf) values (:ID,:name,:age,:weight,:mf)",name,mf,ID,weight,age);
           if(!qry.exec()){
               QMessageBox::critical(this,tr("Save Lions"),tr("Saved"));
               corn.plusAnimals(1);
           }else{
               QMessageBox::critical(this,tr("error"),qry.lastError().text());
           }
           corn.closeDb();
       }
       if(index == 2){
           qry = corn.bdAddSafeAnimals("insert into Bear (ID,name,age,weight,mf) values (:ID,:name,:age,:weight,:mf)",name,mf,ID,weight,age);
           if(!qry.exec()){
               QMessageBox::critical(this,tr("Save Bear"),tr("Saved"));
               corn.plusAnimals(2);
               //corn.closeDb();
           }else{
               QMessageBox::critical(this,tr("error"),qry.lastError().text());
           }
           corn.closeDb();
       }
       if(index == 3){
           qry = corn.bdAddSafeAnimals("insert into Zebra (ID,name,age,weight,mf) values (:ID,:name,:age,:weight,:mf)",name,mf,ID,weight,age);
           if(!qry.exec()){
               QMessageBox::critical(this,tr("Save Zebra"),tr("Saved"));
               corn.plusAnimals(3);
               //corn.closeDb();
           }else{
               QMessageBox::critical(this,tr("error"),qry.lastError().text());
           }
           corn.closeDb();
       }
       if(index == 4){
           qry = corn.bdAddSafeAnimals("insert into Lemur (ID,name,age,weight,mf) values (:ID,:name,:age,:weight,:mf)",name,mf,ID,weight,age);
           if(!qry.exec()){
               QMessageBox::critical(this,tr("Save Lemur"),tr("Saved"));
               corn.plusAnimals(4);
               //corn.closeDb();
           }else{
               QMessageBox::critical(this,tr("error"),qry.lastError().text());
           }
           corn.closeDb();
       }
       if(index == 5){
           qry = corn.bdAddSafeAnimals("insert into Giraffe (ID,name,age,weight,mf) values (:ID,:name,:age,:weight,:mf)",name,mf,ID,weight,age);
           if(!qry.exec()){
               QMessageBox::critical(this,tr("Save Giraffe"),tr("Saved"));
               corn.plusAnimals(5);
           }else{
               QMessageBox::critical(this,tr("error"),qry.lastError().text());

           }
           corn.closeDb();
       }
       if(index == 6){
           qry = corn.bdAddSafeAnimals("insert into Behemoth (ID,name,age,weight,mf) values (:ID,:name,:age,:weight,:mf)",name,mf,ID,weight,age);
           if(!qry.exec()){
               QMessageBox::critical(this,tr("Save Behemoth"),tr("Saved"));
               corn.plusAnimals(6);
               //corn.closeDb();
           }else{
               QMessageBox::critical(this,tr("error"),qry.lastError().text());
           }
           corn.closeDb();
       }
}

void safeAnim::on_pushButton_clicked()
{

    UpdateBase(d);
    this->close();
}
