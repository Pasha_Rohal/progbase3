#ifndef SAFEANIM_H
#define SAFEANIM_H

#include <QDialog>

namespace Ui {
class safeAnim;
}

class safeAnim : public QDialog
{
    Q_OBJECT

public:
    explicit safeAnim(QWidget *parent = 0);
    ~safeAnim();
    void setD(int d);
public:
    int d;
    void UpdateBase(int index);


private slots:
    void on_pushButton_clicked();

private:
    Ui::safeAnim *ui;
};

#endif // SAFEANIM_H
