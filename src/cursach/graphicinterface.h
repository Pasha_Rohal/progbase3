#ifndef GRAPHICINTERFACE_H
#define GRAPHICINTERFACE_H

#include <QDialog>
#include <QSqlQuery>
#include <QMessageBox>
#include "informationanimals.h"
#include <QSqlQueryModel>

namespace Ui {
class graphicInterface;
}

class graphicInterface : public QDialog
{
    Q_OBJECT

public:
    explicit graphicInterface(QWidget *parent = 0);
    ~graphicInterface();
public:
    void inputTable(QString str);
    void fillTable(int index);
    int id;
    void setId(int id);
public:
    QSqlDatabase mydbs;


private:
    Ui::graphicInterface *ui;
};

#endif // GRAPHICINTERFACE_H
