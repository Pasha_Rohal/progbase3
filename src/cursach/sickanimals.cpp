#include "sickanimals.h"
#include "ui_sickanimals.h"


sickAnimals::sickAnimals(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::sickAnimals)
{
    ui->setupUi(this);
    loadfromBd();

}

sickAnimals::~sickAnimals()
{
    delete ui;
}

void sickAnimals::loadfromBd()
{

    QSqlQueryModel *modal = new QSqlQueryModel();
    dialog corn;
    corn.openDb();
    QSqlQuery *qry = new QSqlQuery(mydbs);
    qry->prepare("select * from seekAnimals");
    qry->exec();
    modal->setQuery(*qry);
    ui->tableView->setModel(modal);
    qDebug() <<(modal->rowCount());
    corn.closeDb();
}

void sickAnimals::on_Save_clicked()
{
    bd db;
    //dialog corn;
    int ID,cellNumber,quality;
    QString nameAnimal,healthStatus;

    ID = ui->ID->text().toInt();
    nameAnimal = ui->nameAnimal->text();
    cellNumber = ui->cellNumber->text().toInt();
    quality = ui->quality->text().toInt();
    healthStatus = ui->healthStatus->text();
    db.insert("insert into seekAnimals (ID,NameAnimal,NumberCell,Quality,HealthStatus)"
                          " VALUES (:ID,:NameAnimal,:NumberCell,:Quality,:HealthStatus)",ID,cellNumber,quality,nameAnimal,healthStatus);

     loadfromBd();
}

void sickAnimals::on_upgrade_clicked()
{
    bd db;
    int ID,cellNumber,Quality;
    QString NameAnimal,HealthStatus;

    ID = ui->ID->text().toInt();
    NameAnimal = ui->nameAnimal->text();
    cellNumber = ui->cellNumber->text().toInt();
    Quality = ui->quality->text().toInt();
    HealthStatus = ui->healthStatus->text();
    db.update("UPDATE seekAnimals SET ID=:ID,NameAnimal=:NameAnimal,NumberCell=:cellNumber,Quality=:Quality,HealthStatus=:HealthStatus where ID=:ID",ID,cellNumber,Quality,NameAnimal,HealthStatus);
    loadfromBd();
}

void sickAnimals::on_Delete_clicked()
{
    bd db;
    int  ID;
    ID = ui->ID->text().toInt();
    db.deleter("delete from seekAnimals where ID=:ID",ID);
    loadfromBd();
}
