#include "bd.h"
#include "sickanimals.h"
#include "resursan.h"
#include "QMessageBox"

bd::bd()
{

}

bool bd::openDb()
{
    myDB = QSqlDatabase::addDatabase("QSQLITE");
    myDB.setDatabaseName("/home/pasha/Logins/logins.sqlite");
    if(!myDB.open()){
        qDebug()<<"Failed open";
        return false;
    }else{
        qDebug()<<"Connect...";
        return true;
    }
}

void bd::closeDb()
{
    myDB.close();
    myDB.removeDatabase(QSqlDatabase::defaultConnection);

}

QSqlQuery bd::bdAddSafeAnimals(QString str, QString name, QString mf, int ID, int weight, int age)
{
    QSqlQuery qry;
    qry.prepare(str);
        qry.bindValue(":ID",ID);
            qry.bindValue(":name",name);
                qry.bindValue(":age",age);
                    qry.bindValue(":weight",weight);
                        qry.bindValue(":mf",mf);
                        qry.exec();
                        return qry;
}

QSqlQuery bd::insert(QString str, int ID, int cellNumber, int quality, QString nameAnimal, QString healthStatus)
{
    openDb();
    QSqlQuery qry;
    qry.prepare(str);
    qry.bindValue(":ID", ID);
    qry.bindValue(":NameAnimal",nameAnimal);
    qry.bindValue(":NumberCell",cellNumber);
    qry.bindValue(":Quality",quality);
    qry.bindValue(":HealthStatus",healthStatus);
    qry.exec();
    return qry;
}

QSqlQuery bd::update(QString str, int ID, int CellNumber, int Quality, QString nameAnimal, QString healthStatus)
{
    openDb();
    QSqlQuery qry;
    qry.prepare(str);
    qry.bindValue(":ID", ID);
    qry.bindValue(":NameAnimal",nameAnimal);
    qry.bindValue(":cellNumber",CellNumber);
    qry.bindValue(":Quality",Quality);
    qry.bindValue(":HealthStatus",healthStatus);
    qry.exec();
    return qry;
}

QSqlQuery bd::deleter(QString str, int ID)
{
    openDb();
    QSqlQuery qry;
    qry.prepare(str);
    qry.bindValue(":ID",ID);
    qry.exec();
    return qry;
}

QSqlQuery bd::selectResurse(QString str, int FoodSupplies)
{
    openDb();
    QSqlQuery qry;
    qry.prepare(str);
    if(qry.exec()){
        if(qry.next()){
            FoodSupplies = qry.value(0).toInt()-1;
            qry.prepare("update supplies set FoodSupplies=:addAn");
            qry.bindValue(":addAn",FoodSupplies);
        }
        qry.exec();
        return qry;
    }
}

QSqlQuery bd::updateResuerse(QString str, int Head, int tail)
{
    int ID = 1;
    openDb();
    QSqlQuery qry;
    qry.prepare(str);
    qry.bindValue(":FoodSupplies",Head);
    qry.bindValue(":HealthSupplies",tail);
    qry.bindValue(":ID",ID);
    qry.exec();
    return qry;
}

QSqlQuery bd::deleteDb(QString string, QString name)
{
    openDb();
    QSqlQuery qry;
    qry.prepare(string);
    qry.bindValue(":ID",name);
    qry.exec();
    return qry;
}

QSqlQuery bd::plusAnimals(int ID)
{
    int plusQuery;
    openDb();
    QSqlQuery qry;
    qry.prepare("select quality from Animals where ID=:ID");
    qry.bindValue(":ID",ID);
    if(qry.exec()){
        if(qry.next()){
            plusQuery =  qry.value(0).toInt()+1;
            qry.prepare("update Animals set quality=:plusQuery where ID=:ID");
                qry.bindValue(":ID",ID);
                    qry.bindValue(":plusQuery",plusQuery);
                        qry.exec();
        }else{
            QMessageBox::information(nullptr,"Error","Error");
        }
    }
    closeDb();
    return qry;
}

QSqlQuery bd::thought(QString events, int ID)
{
    openDb();
    QSqlQuery qry;
    qry.prepare("INSERT into events (events,ID)"
                "VALUES(:events,:ID)");
    qry.bindValue(":events",events);
    qry.bindValue(":ID",ID);
    qry.exec();
    closeDb();
}

QSqlQuery bd::dialog(QString user, QString password)
{
    QSqlQuery qry;
    qry.prepare ("select *from Users where Username=:Username and Password=:Password");
    qry.bindValue(":Username",user);
    qry.bindValue(":Password",password);
    if(qry.exec()){
        if(qry.next()){
            QString msg = "Username = " +qry.value(1).toString()+"\n"+
                    "Password = " + qry.value(2).toString()+"\n";
                    QMessageBox::warning(nullptr,"Login was succesful",msg);

        }
    }
    return qry;
}

QSqlQuery bd::load(QSqlQuery qry,QString str)
{
    openDb();
    qry.prepare(str);
    qry.exec();
    return qry;
}

