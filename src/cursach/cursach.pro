#-------------------------------------------------
#
# Project created by QtCreator 2018-05-24T15:58:24
#
#-------------------------------------------------
QT       += core gui sql


greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = cursach
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    bd.cpp \
    dialog.cpp \
    resursan.cpp \
    sickanimals.cpp \
    supplies.cpp \
    animals.cpp \
    informationanimals.cpp \
    number_of_animals.cpp \
    addanimals.cpp \
    safeanim.cpp \
    addaftertimer.cpp \
    graphicinterface.cpp \
    security.cpp

HEADERS  += mainwindow.h \
    bd.h \
    dialog.h \
    resursan.h \
    sickanimals.h \
    supplies.h \
    animals.h \
    informationanimals.h \
    number_of_animals.h \
    addanimals.h \
    safeanim.h \
    addaftertimer.h \
    graphicinterface.h \
    security.h

FORMS    += mainwindow.ui \
    dialog.ui \
    resursan.ui \
    sickanimals.ui \
    supplies.ui \
    animals.ui \
    informationanimals.ui \
    addanimals.ui \
    safeanim.ui \
    addaftertimer.ui \
    graphicinterface.ui \
    security.ui

QMAKE_CXXFLAGS += -std=gnu++14

RESOURCES += \
    resurce.qrc

DISTFILES += \
    image/mainBig.jpg \
    images/31wpO99R9Xo-300x214.jpg
