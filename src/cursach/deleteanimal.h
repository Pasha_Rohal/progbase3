#ifndef DELETEANIMAL_H
#define DELETEANIMAL_H

#include <dialog.h>
#include <QString>

class deleteAnimal
{
public:
    void deleteBox(QString title,QString message);
    void deleteSQ(int k);
    void deleteQuality(QString ID);
    void setK(int k);
public:
    int k;

    deleteAnimal();
};

#endif // DELETEANIMAL_H
