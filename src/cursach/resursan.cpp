#include "resursan.h"
#include "ui_resursan.h"
#include "mainwindow.h"
#include "sickanimals.h"
#include "supplies.h"


resursan::resursan(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::resursan)
{
    ui->setupUi(this);
}

resursan::~resursan()
{
    delete ui;
}

void resursan::on_information_clicked()
{
    MainWindow *w = new MainWindow();
    w->show();
    this->close();
}

void resursan::on_sickAn_clicked()
{
    sickAnimals w;
    w.exec();
}

void resursan::on_eatPb_clicked()
{
    supplies t;
    t.exec();
}
