#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "bd.h"
#include "dialog.h"
#include "QtSql/QSqlDatabase"
#include "QSqlQuery"
#include <QStandardItemModel>
#include <QStandardItem>
#include <QDebug>
#include <QSqlError>
#include <QWidget>
#include <QSqlTableModel>
#include <QDataWidgetMapper>
#include <QMessageBox>
#include <QTimer>
#include <QTime>
#include <addaftertimer.h>
#include "security.h"
#include "graphicinterface.h"
#include <resursan.h>
#include <QCloseEvent>


QTimer *tmr;
QTimer *timer1 = new QTimer();
QTimer *randomNumber = new QTimer();
QTimer *timer2 = new QTimer();


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
        ui->setupUi(this);
        getTimer = new QTimer(this);
        connect(getTimer,SIGNAL(timeout()),this,SLOT(birthdayAnm()));
        //connect(getTimer,SIGNAL(timeout()),this,SLOT(deleteAnm()));
        getTimer->start(1000);
        connect(randomNumber,SIGNAL(timeout()),this,SLOT(onTimeOut()));
        tmr = new QTimer(this); // Создаем объект класса QTimer и передаем адрес переменной
        tmr->setInterval(1000);
        connect(tmr, SIGNAL(timeout()), this, SLOT(updateTime())); // Подключаем сигнал таймера к нашему слоту
        connect (tmr,SIGNAL(timeout()),this,SLOT(loadfromBd()));
        tmr->start(); // Запускаем таймер
        connect(timer1,SIGNAL(timeout()),this,SLOT(incremental()));
        connect(tmr, SIGNAL(timeout()), this, SLOT(updateTime()));
        connect(timer2,SIGNAL(timeout()),this,SLOT(timerForNewAnimal()));
        timerWorks();
        //loadfromBd();
}

MainWindow::~MainWindow()
{
    delete ui;
    delete tmr;


}



void MainWindow::closeEvent(QCloseEvent *event)
{
    QMessageBox::StandardButton resBtn = QMessageBox::question( this, "Exit",
                                                                    tr("Are you sure?\n"),
                                                                    QMessageBox::No | QMessageBox::Yes,
                                                                    QMessageBox::Yes);
        if (resBtn != QMessageBox::Yes) {
            event->ignore();
        } else {
            resursan *t = new resursan();
            t->show();
            event->accept();
        }

}

void MainWindow::birthdayAnm()
{
    bd db;
    srand(time(0));
    int k = rand()%1500;
    if(k == 1){
        db.thought("Lion",0);
    }
    if(k == 3){
        db.thought("Zebra",0);
    }
    if(k == 5){
        db.thought("Giraffe",0);
    }
    if(k == 4){
        db.thought("Lemur",0);
    }
    if(k == 6){
        db.thought("Behemoth",0);
    }
    if(k == 2){
        db.thought("Bear",0);
    }

}


void MainWindow::updateTime()
{
    ui->label_7->setText(QTime::currentTime().toString());
}

void MainWindow::incremental(){
    supplies supp;
    int aux;int aux2;int aux3;int aux4;int aux5;int aux6;
    aux = ui->progressBar->value();
    aux2 = ui->progressBar_2->value();
    aux3 = ui->progressBar_3->value();
    aux4 = ui->progressBar_4->value();
    aux5 = ui->progressBar_5->value();
    aux6 = ui->progressBar_6->value();
    if(aux == 100){
        supp.useResources();
        aux = 0;
    }else{
     aux++;
    }
    if(aux2 == 100){
        supp.useResources();
        aux2 = 0;
    }else{
     aux2++;
    }
    if(aux3 == 100){
        supp.useResources();
        aux3 = 0;
    }else{
     aux3++;
    }
    if(aux4 == 100){
        supp.useResources();
        aux4 = 0;
    }else{
     aux4++;
    }
    if(aux5 == 100){
        supp.useResources();
        aux5 = 0;
    }else{
     aux5++;
    }
    if(aux6 == 100){
        supp.useResources();
        aux6 = 0;
    }else{
     aux6++;
    }
    ui->progressBar->setValue(aux);
    ui->progressBar_2->setValue(aux2);
    ui->progressBar_3->setValue(aux3);
    ui->progressBar_4->setValue(aux4);
    ui->progressBar_5->setValue(aux5);
    ui->progressBar_6->setValue(aux6);

}

void MainWindow::timerForNewAnimal()
{
    bd db;
    int timer;
    timer =ui->newAnimal->value();

    if(timer == 100){
        timer = 0;
        db.thought("comming",1);
    }else{
        timer++;
    }
    ui->newAnimal->setValue(timer);
}

void MainWindow:: timerWorks(){
    randomNumber->start(3000);
    timer1->start(3000);
    ui->startTime->setText("Database");
}

void MainWindow::onTimeOut()
{
    ui->peoplesCome->setText(QString::number(rand()%100+400));

}

void MainWindow::on_startTime_clicked()
{
    Animals w;
    w.exec();

}

void MainWindow::on_change_clicked()
{
    security t(this);
    if(t.exec() == QDialog::Accepted){
        ui->securityLabel->setText(t.getText());
    }

}

void MainWindow::on_LionButton_clicked()
{
    safeAnim t;
    t.setD(1);
    t.exec();
}

void MainWindow::on_BearButton_clicked()
{
    safeAnim t;
    t.setD(2);
    t.exec();
}

void MainWindow::on_ZebraButton_clicked()
{
    safeAnim t;
    t.setD(3);
    t.exec();
}

void MainWindow::on_LemurButton_clicked()
{
    safeAnim t;
    t.setD(4);
    t.exec();

}

void MainWindow::on_GiraffeButton_clicked()
{
    safeAnim t;
    t.setD(5);
    t.exec();
}

void MainWindow::on_BehemothButton_clicked()
{
    safeAnim t;
    t.setD(6);
    t.exec();
}

void MainWindow::on_timerButton_clicked()
{
        if(timer2->isActive()){
            timer2->stop();
           ui->timerButton->setText("start");
        }else{
            timer2->start(5000);
            ui->timerButton->setText("pause");
        }

}



void MainWindow::on_graficLemur_clicked()
{
    graphicInterface t;
    t.setId(4);
    t.exec();
}

void MainWindow::on_graficLion_clicked()
{
    graphicInterface t;
    t.setId(1);
    t.exec();


}

void MainWindow::on_graficBehomoth_clicked()
{
    graphicInterface t;
    t.setId(6);
    t.exec();

}

void MainWindow::on_graficBear_clicked()
{
    graphicInterface t;
    t.setId(2);
    t.exec();

}

void MainWindow::on_graficZebra_clicked()
{
    graphicInterface ad(this);
    ad.setId(3);
    ad.exec();


}

void MainWindow::on_graficGiraffe_clicked()
{
    graphicInterface t;
    t.setId(5);
    t.exec();

}

void MainWindow::loadfromBd()
{
    QSqlQueryModel *modal = new QSqlQueryModel();
    dialog corn;
    corn.openDb();
    QSqlQuery *qry = new QSqlQuery(mydbs);
    qry->prepare("select * from events");
    qry->exec();
    modal->setQuery(*qry);
    ui->tableView->setModel(modal);
    ui->tableView->setColumnHidden(1,true);
    qDebug() <<(modal->rowCount());
    corn.closeDb();
}

void MainWindow::on_tableView_activated(const QModelIndex &index)
{
    bd db;
    QString val = ui->tableView->model()->data(index).toString();
    if(val == "Lion"){
            addAnimals t;
            t.setK(1);
            t.exec();
           db.deleteDb("DELETE from events WHERE events=:ID",val);
        }
    if(val == "Bear"){
            addAnimals t;
            t.setK(2);
            t.exec();
           db.deleteDb("DELETE from events WHERE events=:ID",val);
        }
    if(val == "Zebra"){
            addAnimals t;
            t.setK(3);
            t.exec();
           db.deleteDb("DELETE from events WHERE events=:ID",val);
        }
    if(val == "Lemur"){
            addAnimals t;
            t.setK(4);
            t.exec();
           db.deleteDb("DELETE from events WHERE events=:ID",val);
        }
    if(val == "Giraffe"){
            addAnimals t;
            t.setK(5);
            t.exec();
           db.deleteDb("DELETE from events WHERE events=:ID",val);
        }
    if(val == "Behemoth"){
            addAnimals t;
            t.setK(6);
            t.exec();
           db.deleteDb("DELETE from events WHERE events=:ID",val);
        }
    if(val == "comming"){
            addaftertimer t;
            t.exec();
           db.deleteDb("DELETE from events WHERE events=:ID",val);
        }
}
