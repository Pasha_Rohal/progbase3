#include "graphicinterface.h"
#include "ui_graphicinterface.h"
#include "bd.h"

graphicInterface::graphicInterface(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::graphicInterface)
{
    ui->setupUi(this);
    //fillTable(id);
}

graphicInterface::~graphicInterface()
{
    delete ui;
}

void graphicInterface::inputTable(QString str)
{
    QSqlQueryModel *modal = new QSqlQueryModel();
    bd corn;
    corn.openDb();
    QSqlQuery *qry = new QSqlQuery(mydbs);
    qry->prepare(str);
    qry->exec();
    modal->setQuery(*qry);
    ui->tableView->setModel(modal);
    qDebug() <<(modal->rowCount());
    corn.closeDb();
}

void graphicInterface::setId(int id)
{
    this->id = id;
    fillTable(id);
}

void graphicInterface::fillTable(int id)
{

    if(id == 1){
        inputTable("select * from Lions");
    }
    if(id == 2){
        inputTable("select * from Bear");
    }
    if(id == 3){
        inputTable("select * from Zebra");
    }
    if(id == 4){
        inputTable("select * from Lemur");
    }
    if(id == 5){
        inputTable("select * from Giraffe");
    }
    if(id == 6){
        inputTable("select * from Behemoth");
    }
}
