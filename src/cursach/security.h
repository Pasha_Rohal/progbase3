#ifndef SECURITY_H
#define SECURITY_H

#include <QDialog>
#include "dialog.h"
#include <QMessageBox>
#include <QSqlDatabase>

namespace Ui {
class security;
}

class security : public QDialog
{
    Q_OBJECT

public:
    explicit security(QWidget *parent = 0);
    ~security();

    QSqlDatabase mydbs;
    QString getText();

    void loadBdSecurity();

private slots:
    void on_security_2_activated(const QModelIndex &index);

private:
    Ui::security *ui;
    QString text;
};

#endif // SECURITY_H
