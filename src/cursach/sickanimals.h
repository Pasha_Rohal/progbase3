#ifndef SICKANIMALS_H
#define SICKANIMALS_H

#include <QDialog>
#include <QMessageBox>
#include <QDebug>

#include "dialog.h"
#include "animals.h"
#include "bd.h"

namespace Ui {
class sickAnimals;
}

class sickAnimals : public QDialog
{
    Q_OBJECT

public:
    explicit sickAnimals(QWidget *parent = 0);
    ~sickAnimals();
public:
    QSqlDatabase mydbs;

    void loadfromBd();

private slots:
    void on_Save_clicked();

    void on_upgrade_clicked();

    //void on_pushButton_clicked();

    void on_Delete_clicked();

private:
    int id;
    Ui::sickAnimals *ui;
};

#endif // SICKANIMALS_H
