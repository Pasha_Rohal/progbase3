#ifndef SUPPLIES_H
#define SUPPLIES_H

#include <QDialog>
#include "dialog.h"
#include <QMessageBox>

#include "bd.h"


namespace Ui {
class supplies;
}

class supplies : public QDialog
{
    Q_OBJECT

public:
    explicit supplies(QWidget *parent = 0);
    ~supplies();
public:
    void useResources();


private slots:
    void on_UpdateB_clicked();

    void on_FoodE_textChanged(const QString &arg1);

    void on_HealthE_textChanged(const QString &arg1);

private:
    Ui::supplies *ui;
    QSqlDatabase mydbs;

private:
    void validForm();
    void loadFromBd2();
};

#endif // SUPPLIES_H
