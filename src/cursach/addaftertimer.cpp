#include "addaftertimer.h"
#include "ui_addaftertimer.h"
#include <safeanim.h>

addaftertimer::addaftertimer(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::addaftertimer)
{
    ui->setupUi(this);

}

addaftertimer::~addaftertimer()
{
    delete ui;
}

void addaftertimer::on_LionPush_clicked()
{
    safeAnim t;
    t.setD(1);
    t.exec();
}

void addaftertimer::on_BearPush_clicked()
{
    safeAnim t;
    t.setD(2);
    t.exec();
}

void addaftertimer::on_ZebraPush_clicked()
{
    safeAnim t;
    t.setD(3);
    t.exec();
}

void addaftertimer::on_LemurPush_clicked()
{
    safeAnim t;
    t.setD(4);
    t.exec();
}

void addaftertimer::on_GiraffePush_clicked()
{
    safeAnim t;
    t.setD(5);
    t.exec();
}

void addaftertimer::on_BehemothPush_clicked()
{
    safeAnim t;
    t.setD(6);
    t.exec();
}
