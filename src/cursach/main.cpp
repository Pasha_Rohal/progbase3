#include "mainwindow.h"
#include <QApplication>
#include <QTimer>
#include <QDialog>
#include "dialog.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    dialog d;
    d.show();

    return a.exec();
}
