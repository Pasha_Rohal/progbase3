#include "supplies.h"
#include "ui_supplies.h"
#include "dialog.h"
#include "bd.h"
#include "mainwindow.h"

supplies::supplies(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::supplies)
{
    ui->setupUi(this);
    loadFromBd2();
    validForm();

}
void supplies::loadFromBd2()
{
    QSqlQueryModel *modal = new QSqlQueryModel();
    bd corn;
    corn.openDb();
    QSqlQuery *qry = new QSqlQuery(mydbs);
    qry->prepare("select * from supplies");
    qry->exec();
    modal->setQuery(*qry);
    ui->tableView->setModel(modal);
    qDebug() <<(modal->rowCount());
    corn.closeDb();
}

supplies::~supplies()
{
    delete ui;
}

void supplies::useResources()
{
    int FoodSupplies;
    bd corn;
    corn.selectResurse("select FoodSupplies from supplies",FoodSupplies);
    loadFromBd2();
}

void supplies::on_UpdateB_clicked()
{
    bd corn;
    int FoodSupplies,HealthSupplies;
    FoodSupplies = ui->FoodE->text().toInt();
    HealthSupplies = ui->HealthE->text().toInt();
    corn.updateResuerse("update supplies set FoodSupplies =:FoodSupplies,HealthSupplies=:HealthSupplies where ID=:ID",FoodSupplies,HealthSupplies);
    loadFromBd2();
}

void supplies::validForm()
{
    bool valid = !ui->FoodE->text().isEmpty()&& !ui->HealthE->text().isEmpty();
    ui->UpdateB->setEnabled(valid);
}

void supplies::on_FoodE_textChanged(const QString &arg1)
{
    validForm();
}

void supplies::on_HealthE_textChanged(const QString &arg1)
{
    validForm();
}
