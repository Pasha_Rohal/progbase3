#include "addanimals.h"
#include "ui_addanimals.h"
#include <QErrorMessage>
#include <number_of_animals.h>
#include <bd.h>

addAnimals::addAnimals(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::addAnimals)
{
    ui->setupUi(this);
}

addAnimals::~addAnimals()
{
    delete ui;
}

void addAnimals::setK(int k)
{
    this->k=k;
}

void addAnimals::UpdateBase(int index)
{

    bd db;
    dialog corn(this);
    QString name,mf;
    int ID,weight,age;

    age = ui->ageLineEdit->text().toInt();
    name = ui->nameEdit->text();
    weight = ui->weighEdit->text().toInt();
    mf = ui->feEdit->text();
    ID = ui->ageB->text().toInt();
    if(!corn.openDb()){
        qDebug()<<"Failed to open Database";
        return;
    }
    QSqlQuery qry;
    if(index == 1){
    qry = db.bdAddSafeAnimals("insert into Lions (ID,name,age,weight,mf) values (:ID,:name,:age,:weight,:mf)",name,mf,ID,weight,age);

    if(qry.exec()){
        QMessageBox::critical(this,tr("Save Lions"),tr("Saved"));
        db.plusAnimals(1);
    }else{
        QMessageBox::critical(this,tr("error"),qry.lastError().text());

    }
    db.closeDb();
}
    if(index == 2){
        qry = db.bdAddSafeAnimals("insert into Bear (ID,name,age,weight,mf) values (:ID,:name,:age,:weight,:mf)",name,mf,ID,weight,age);

        if(qry.exec()){
            QMessageBox::critical(this,tr("Save Bear"),tr("Saved"));
            db.plusAnimals(2);
            //corn.closeDb();
        }else{
            QMessageBox::critical(this,tr("error"),qry.lastError().text());

        }
        db.closeDb();
    }
    if(index == 3){
        qry = db.bdAddSafeAnimals("insert into Zebra (ID,name,age,weight,mf) values (:ID,:name,:age,:weight,:mf)",name,mf,ID,weight,age);

        if(qry.exec()){
            QMessageBox::critical(this,tr("Save Zebra"),tr("Saved"));
            db.plusAnimals(3);
            //corn.closeDb();
        }else{
            QMessageBox::critical(this,tr("error"),qry.lastError().text());

        }
        db.closeDb();
    }
    if(index == 4){
        qry = db.bdAddSafeAnimals("insert into Lemur (ID,name,age,weight,mf) values (:ID,:name,:age,:weight,:mf)",name,mf,ID,weight,age);

        if(qry.exec()){
            db.plusAnimals(4);
            QMessageBox::critical(this,tr("Save Lemur"),tr("Saved"));
            //corn.closeDb();
        }else{
            QMessageBox::critical(this,tr("error"),qry.lastError().text());

        }
        db.closeDb();
    }
    if(index == 5){
        qry = db.bdAddSafeAnimals("insert into Giraffe (ID,name,age,weight,mf) values (:ID,:name,:age,:weight,:mf)",name,mf,ID,weight,age);

        if(qry.exec()){
            db.plusAnimals(5);
            QMessageBox::critical(this,tr("Save Giraffe"),tr("Saved"));
            //corn.closeDb();
        }else{
            QMessageBox::critical(this,tr("error"),qry.lastError().text());

        }
        db.closeDb();
    }
    if(index == 6){
        qry = db.bdAddSafeAnimals("insert into Behemoth (ID,name,age,weight,mf) values (:ID,:name,:age,:weight,:mf)",name,mf,ID,weight,age);

        if(qry.exec()){
            QMessageBox::critical(this,tr("Save Behemoth"),tr("Saved"));
            db.plusAnimals(6);
            //corn.closeDb();
        }else{
            QMessageBox::critical(this,tr("error"),qry.lastError().text());

        }
        db.closeDb();
    }
}

void addAnimals::on_Create_clicked()
{

    UpdateBase(k);
    this->close();
}
