#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include<QTime>
#include<QTimer>
#include<animals.h>
#include <iostream>
#include <number_of_animals.h>
#include <QMessageBox>
#include <addanimals.h>
#include <safeanim.h>
#include <supplies.h>
#include "informationanimals.h"
#include "security.h"
#include <QCloseEvent>



namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
public:
    QTimer *getTimer;
    QSqlDatabase mydbs;


public slots:
    void closeEvent (QCloseEvent *event);
    void birthdayAnm();
    void timerWorks();
    void loadfromBd();
    void onTimeOut();
private slots:
    void incremental();
    void timerForNewAnimal();
    void updateTime();
    void on_startTime_clicked();

    void on_change_clicked();

    void on_BearButton_clicked();

    void on_LionButton_clicked();

    void on_ZebraButton_clicked();

    void on_BehemothButton_clicked();

    void on_LemurButton_clicked();

    void on_GiraffeButton_clicked();



    void on_timerButton_clicked();

    void on_graficLemur_clicked();

    void on_graficLion_clicked();

    void on_graficBehomoth_clicked();

    void on_graficBear_clicked();

    void on_graficZebra_clicked();

    void on_graficGiraffe_clicked();

    void on_tableView_activated(const QModelIndex &index);

private:
    Ui::MainWindow *ui;
    QTimer *tmr;
};

#endif // MAINWINDOW_H
