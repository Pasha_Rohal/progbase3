#include "dialog.h"
#include "ui_dialog.h"
#include "mainwindow.h"
#include <QMessageBox>
#include <QDebug>
#include "resursan.h"


dialog::dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dialog)
{
    ui->setupUi(this);

    openDb();
    QFileInfo checkFile("/home/pasha/Logins/logins.sqlite");

    if(checkFile.isFile()){
        if(myDB.open()){
            ui->label_3->setText("[+]Connect to Database file");
        }
    }else{
        ui->label_3->setText("[-]Dont connect to Database file");
    }
}

dialog::~dialog()
{
    delete ui;
    //qDebug << "Closing to connection to Database file on exit.";
    //myDB.close();
}

bool dialog::openDb()
{
    myDB = QSqlDatabase::addDatabase("QSQLITE");
    myDB.setDatabaseName("/home/pasha/Logins/logins.sqlite");
    if(!myDB.open()){
        qDebug()<<"Failed open";
        return false;
    }else{
        qDebug()<<"Connect...";
        return true;
    }
}

void dialog::closeDb()
{
    myDB.close();
    myDB.removeDatabase(QSqlDatabase::defaultConnection);

}

void dialog::on_btnClear_clicked()
{
    ui->txtPass->setText("");

    ui->txtUser->setText("");
}

void dialog::on_btnLog_clicked()
{
    QString Username,Password;
    Username = ui->txtUser->text();
    Password = ui->txtPass->text();
    bd db;
    db.dialog(Username,Password);
    resursan r;
    this->close();
    r.exec();

}
