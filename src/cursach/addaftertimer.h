#ifndef ADDAFTERTIMER_H
#define ADDAFTERTIMER_H

#include <QDialog>

namespace Ui {
class addaftertimer;
}

class addaftertimer : public QDialog
{
    Q_OBJECT

public:
    explicit addaftertimer(QWidget *parent = 0);
    ~addaftertimer();

private slots:
    void on_LionPush_clicked();

    void on_BearPush_clicked();

    void on_ZebraPush_clicked();

    void on_LemurPush_clicked();

    void on_GiraffePush_clicked();

    void on_BehemothPush_clicked();

private:
    Ui::addaftertimer *ui;
};

#endif // ADDAFTERTIMER_H
