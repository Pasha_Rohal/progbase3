#ifndef DBFUNC_H
#define DBFUNC_H

#include <QtSql>
#include <QDebug>
#include <QFileInfo>
#include <mainwindow.h>

class dbfunc
{
public:
    dbfunc();
    QSqlDatabase db;
    void connClose();
    void connOpen();
};

#endif // DBFUNC_H
