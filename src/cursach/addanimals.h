#ifndef ADDANIMALS_H
#define ADDANIMALS_H


#include <QDialog>
#include <dialog.h>
#include <QMessageBox>

namespace Ui {
class addAnimals;
}

class addAnimals : public QDialog
{
    Q_OBJECT

public:
    explicit addAnimals(QWidget *parent = 0);
    ~addAnimals();
    void setK(int k);
public:
    int k;
    void UpdateBase(int index);

private slots:
    void on_Create_clicked();

private:
    Ui::addAnimals *ui;
};

#endif // ADDANIMALS_H
