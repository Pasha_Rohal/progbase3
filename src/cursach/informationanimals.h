#ifndef INFORMATIONANIMALS_H
#define INFORMATIONANIMALS_H

#include <QDialog>
#include"dialog.h"
#include <QMessageBox>
#include <QLineEdit>
#include <QTableView>

namespace Ui {
class informationAnimals;
}

class informationAnimals : public QDialog
{
    Q_OBJECT

public:
    explicit informationAnimals(QWidget *parent = 0);
    ~informationAnimals();

public:
    QSqlDatabase myDB;
private:
    Ui::informationAnimals *ui;
};

#endif // INFORMATIONANIMALS_H
