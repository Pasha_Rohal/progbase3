#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <QDebug>
#include <QtSql>
#include <QFile>
#include <QFileInfo>


namespace Ui {
class dialog;
}

class dialog : public QDialog
{
    Q_OBJECT

public:
    explicit dialog(QWidget *parent = 0);
    ~dialog();
public:
    QSqlDatabase myDB;
    bool openDb();
    void closeDb();

public slots:


private slots:


    void on_btnClear_clicked();

    void on_btnLog_clicked();

private:
    Ui::dialog *ui;


};

#endif // DIALOG_H
