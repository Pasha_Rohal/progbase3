#ifndef BD_H
#define BD_H

#include <QDialog>
#include <QDebug>
#include <QtSql>
#include <QFile>
#include <QFileInfo>
#include <QWidget>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlTableModel>

class bd
{
public:
    bd();
    QSqlQuery mydbs;
    QSqlDatabase myDB;
    bool openDb();
    void closeDb();
    QSqlQuery bdAddSafeAnimals(QString str,QString name,QString mf,int ID,int weight,int age);
    QSqlQuery insert(QString str,int ID,int cellNumber,int quality,QString nameAnimal,QString healthStatus);
    QSqlQuery update(QString str,int ID,int CellNumber,int Quality,QString nameAnimal,QString healthStatus);
    QSqlQuery deleter(QString str,int ID);
    QSqlQuery selectResurse(QString str,int FoodSupplies);
    QSqlQuery updateResuerse(QString str,int Head,int tail);
    QSqlQuery deleteDb(QString string,QString name);
    QSqlQuery plusAnimals(int ID);
    QSqlQuery thought(QString events,int ID);
    QSqlQuery dialog (QString user,QString password);
    QSqlQuery load(QSqlQuery qry,QString str);
private:

};


#endif // BD_H
