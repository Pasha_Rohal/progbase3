#ifndef ANIMALS_H
#define ANIMALS_H

#include <QDialog>
#include "dialog.h"
#include <QMessageBox>
#include "informationanimals.h"
#include <QSqlQuery>

#define lions "select * from Lions"

namespace Ui {
class Animals;
}

class Animals : public QDialog
{
    Q_OBJECT

public:
    explicit Animals(QWidget *parent = 0);
    ~Animals();
public:
     int getId();
    QSqlDatabase mydbs;
    void togetherFun(QString  str);
    void loadFromBd3();

private slots:
    void on_tableView_activated(const QModelIndex &index);


private:
    Ui::Animals *ui;
    int id;
};

#endif // ANIMALS_H
