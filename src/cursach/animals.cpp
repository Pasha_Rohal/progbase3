#include "animals.h"
#include "ui_animals.h"
#include "bd.h"

Animals::Animals(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Animals)
{
    ui->setupUi(this);
    loadFromBd3();
}

Animals::~Animals()
{
    delete ui;

}

void Animals::togetherFun(QString  str)
{
    QSqlQueryModel *modal = new QSqlQueryModel();
    bd corn;
    corn.openDb();
    QSqlQuery *qry = new QSqlQuery(mydbs);
    qry->prepare(str);
    qry->exec();
    modal->setQuery(*qry);
    ui->tableView->setModel(modal);
    qDebug() <<(modal->rowCount());
    corn.closeDb();
}



void Animals::loadFromBd3()
{
    QSqlQueryModel *modal = new QSqlQueryModel();
    bd corn;
    corn.openDb();
    QSqlQuery qry;
    qry.prepare("select * from Animals");
    qry.exec();
    modal->setQuery(qry);
    ui->tableView->setModel(modal);
    qDebug() <<(modal->rowCount());
    corn.closeDb();
}

void Animals::on_tableView_activated(const QModelIndex &index)
{
    this->id = index.row()+1;

    switch(this->id)
    {
        case 1:{
           togetherFun("select * from Lions");
            break;
        }

        case 2:{
            togetherFun("select * from Bear");
            break;
        }
        case 3:{
            togetherFun("select * from Zebra");
            break;
        }
        case 4:{
            togetherFun("select * from Lemur");
            break;
        }
        case 5:{
            togetherFun("select * from Giraffe");
            break;
        }
        case 6:{
            togetherFun("select * from Behemoth");
            break;
        }
    }
}
